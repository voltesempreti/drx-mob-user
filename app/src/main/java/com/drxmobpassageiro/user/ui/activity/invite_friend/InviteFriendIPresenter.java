package com.drxmobpassageiro.user.ui.activity.invite_friend;

import com.drxmobpassageiro.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
