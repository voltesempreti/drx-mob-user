package com.drxmobpassageiro.user.ui.activity.coupon;

import com.drxmobpassageiro.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
