package com.drxmobpassageiro.user.ui.activity.passbook;

import com.drxmobpassageiro.user.base.MvpPresenter;

public interface WalletHistoryIPresenter<V extends WalletHistoryIView> extends MvpPresenter<V> {
    void wallet();
}
