package com.drxmobpassageiro.user.ui.activity.wallet;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.AddWallet;
import com.drxmobpassageiro.user.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
    void onSuccess(WalletResponse response);
}
