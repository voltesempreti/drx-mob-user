package com.drxmobpassageiro.user.ui.activity.notification_manager;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}