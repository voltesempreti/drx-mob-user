package com.drxmobpassageiro.user.ui.fragment.service;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.Service;

import java.util.List;

public interface ServiceTypesIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onError(Throwable e);

    void onSuccess(Object object);
}
