package com.drxmobpassageiro.user.ui.activity.invite_friend;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
