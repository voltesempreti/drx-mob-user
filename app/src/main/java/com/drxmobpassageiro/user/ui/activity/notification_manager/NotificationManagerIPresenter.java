package com.drxmobpassageiro.user.ui.activity.notification_manager;

import com.drxmobpassageiro.user.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
