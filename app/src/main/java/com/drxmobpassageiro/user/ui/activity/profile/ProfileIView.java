package com.drxmobpassageiro.user.ui.activity.profile;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.User;

public interface ProfileIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
