package com.drxmobpassageiro.user.ui.fragment.service_flow;

import com.drxmobpassageiro.user.base.MvpPresenter;

public interface ServiceFlowIPresenter<V extends ServiceFlowIView> extends MvpPresenter<V> {

}
