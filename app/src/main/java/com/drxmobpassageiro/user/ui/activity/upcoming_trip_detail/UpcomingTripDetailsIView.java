package com.drxmobpassageiro.user.ui.activity.upcoming_trip_detail;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
