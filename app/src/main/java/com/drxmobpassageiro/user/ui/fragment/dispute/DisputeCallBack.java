package com.drxmobpassageiro.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
