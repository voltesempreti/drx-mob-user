package com.drxmobpassageiro.user.ui.activity.setting;

import com.drxmobpassageiro.user.base.MvpView;
import com.drxmobpassageiro.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
