package com.drxmobpassageiro.user.ui.activity.card;

import com.drxmobpassageiro.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
