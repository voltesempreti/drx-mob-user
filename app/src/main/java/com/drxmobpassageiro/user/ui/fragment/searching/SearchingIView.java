package com.drxmobpassageiro.user.ui.fragment.searching;

import com.drxmobpassageiro.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
