/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.firebase.ui.storage;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int ambientEnabled = 0x7f04002a;
        public static final int buttonSize = 0x7f040069;
        public static final int cameraBearing = 0x7f04006e;
        public static final int cameraMaxZoomPreference = 0x7f04006f;
        public static final int cameraMinZoomPreference = 0x7f040070;
        public static final int cameraTargetLat = 0x7f040071;
        public static final int cameraTargetLng = 0x7f040072;
        public static final int cameraTilt = 0x7f040073;
        public static final int cameraZoom = 0x7f040074;
        public static final int circleCrop = 0x7f04009d;
        public static final int colorScheme = 0x7f0400ce;
        public static final int font = 0x7f04017e;
        public static final int fontProviderAuthority = 0x7f040180;
        public static final int fontProviderCerts = 0x7f040181;
        public static final int fontProviderFetchStrategy = 0x7f040182;
        public static final int fontProviderFetchTimeout = 0x7f040183;
        public static final int fontProviderPackage = 0x7f040184;
        public static final int fontProviderQuery = 0x7f040185;
        public static final int fontStyle = 0x7f040186;
        public static final int fontWeight = 0x7f040188;
        public static final int imageAspectRatio = 0x7f0401a5;
        public static final int imageAspectRatioAdjust = 0x7f0401a6;
        public static final int latLngBoundsNorthEastLatitude = 0x7f0401c8;
        public static final int latLngBoundsNorthEastLongitude = 0x7f0401c9;
        public static final int latLngBoundsSouthWestLatitude = 0x7f0401ca;
        public static final int latLngBoundsSouthWestLongitude = 0x7f0401cb;
        public static final int liteMode = 0x7f04021d;
        public static final int mapType = 0x7f040231;
        public static final int scopeUris = 0x7f04027f;
        public static final int uiCompass = 0x7f04032a;
        public static final int uiMapToolbar = 0x7f04032b;
        public static final int uiRotateGestures = 0x7f04032c;
        public static final int uiScrollGestures = 0x7f04032d;
        public static final int uiTiltGestures = 0x7f04032f;
        public static final int uiZoomControls = 0x7f040330;
        public static final int uiZoomGestures = 0x7f040331;
        public static final int useViewLifecycle = 0x7f040334;
        public static final int zOrderOnTop = 0x7f040344;
    }
    public static final class bool {
        private bool() {}

        public static final int abc_action_bar_embed_tabs = 0x7f050000;
    }
    public static final class color {
        private color() {}

        public static final int common_google_signin_btn_text_dark = 0x7f060082;
        public static final int common_google_signin_btn_text_dark_default = 0x7f060083;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f060084;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f060085;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f060086;
        public static final int common_google_signin_btn_text_light = 0x7f060087;
        public static final int common_google_signin_btn_text_light_default = 0x7f060088;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f060089;
        public static final int common_google_signin_btn_text_light_focused = 0x7f06008a;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f06008b;
        public static final int common_google_signin_btn_tint = 0x7f06008c;
        public static final int notification_action_color_filter = 0x7f060199;
        public static final int notification_icon_bg_color = 0x7f06019a;
        public static final int notification_material_background_media_default_color = 0x7f06019b;
        public static final int primary_text_default_material_dark = 0x7f0601e3;
        public static final int ripple_material_light = 0x7f06035c;
        public static final int secondary_text_default_material_dark = 0x7f06035d;
        public static final int secondary_text_default_material_light = 0x7f06035e;
    }
    public static final class dimen {
        private dimen() {}

        public static final int compat_button_inset_horizontal_material = 0x7f070392;
        public static final int compat_button_inset_vertical_material = 0x7f070393;
        public static final int compat_button_padding_horizontal_material = 0x7f070394;
        public static final int compat_button_padding_vertical_material = 0x7f070395;
        public static final int compat_control_corner_material = 0x7f070396;
        public static final int notification_action_icon_size = 0x7f070477;
        public static final int notification_action_text_size = 0x7f070478;
        public static final int notification_big_circle_margin = 0x7f070479;
        public static final int notification_content_margin_start = 0x7f07047a;
        public static final int notification_large_icon_height = 0x7f07047b;
        public static final int notification_large_icon_width = 0x7f07047c;
        public static final int notification_main_column_padding_top = 0x7f07047d;
        public static final int notification_media_narrow_margin = 0x7f07047e;
        public static final int notification_right_icon_size = 0x7f07047f;
        public static final int notification_right_side_padding_top = 0x7f070480;
        public static final int notification_small_icon_background_padding = 0x7f070481;
        public static final int notification_small_icon_size_as_large = 0x7f070482;
        public static final int notification_subtext_size = 0x7f070483;
        public static final int notification_top_pad = 0x7f070485;
        public static final int notification_top_pad_large_text = 0x7f070486;
    }
    public static final class drawable {
        private drawable() {}

        public static final int common_full_open_on_phone = 0x7f0800a9;
        public static final int common_google_signin_btn_icon_dark = 0x7f0800aa;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f0800ab;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f0800ac;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f0800ad;
        public static final int common_google_signin_btn_icon_disabled = 0x7f0800ae;
        public static final int common_google_signin_btn_icon_light = 0x7f0800af;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f0800b0;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f0800b1;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f0800b2;
        public static final int common_google_signin_btn_text_dark = 0x7f0800b3;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f0800b4;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f0800b5;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f0800b6;
        public static final int common_google_signin_btn_text_disabled = 0x7f0800b7;
        public static final int common_google_signin_btn_text_light = 0x7f0800b8;
        public static final int common_google_signin_btn_text_light_focused = 0x7f0800b9;
        public static final int common_google_signin_btn_text_light_normal = 0x7f0800ba;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f0800bb;
        public static final int googleg_disabled_color_18 = 0x7f0800c9;
        public static final int googleg_standard_color_18 = 0x7f0800ca;
        public static final int notification_action_background = 0x7f08017a;
        public static final int notification_bg = 0x7f08017b;
        public static final int notification_bg_low = 0x7f08017c;
        public static final int notification_bg_low_normal = 0x7f08017d;
        public static final int notification_bg_low_pressed = 0x7f08017e;
        public static final int notification_bg_normal = 0x7f08017f;
        public static final int notification_bg_normal_pressed = 0x7f080180;
        public static final int notification_icon_background = 0x7f080181;
        public static final int notification_template_icon_bg = 0x7f080182;
        public static final int notification_template_icon_low_bg = 0x7f080183;
        public static final int notification_tile_bg = 0x7f080184;
        public static final int notify_panel_notification_icon_bg = 0x7f080185;
    }
    public static final class id {
        private id() {}

        public static final int action0 = 0x7f0a002c;
        public static final int action_container = 0x7f0a0034;
        public static final int action_divider = 0x7f0a0037;
        public static final int action_image = 0x7f0a0039;
        public static final int action_text = 0x7f0a0041;
        public static final int actions = 0x7f0a0042;
        public static final int adjust_height = 0x7f0a004b;
        public static final int adjust_width = 0x7f0a004c;
        public static final int async = 0x7f0a0058;
        public static final int auto = 0x7f0a0059;
        public static final int blocking = 0x7f0a0060;
        public static final int button = 0x7f0a006f;
        public static final int cancel_action = 0x7f0a0073;
        public static final int center = 0x7f0a0081;
        public static final int chronometer = 0x7f0a0090;
        public static final int dark = 0x7f0a00df;
        public static final int date = 0x7f0a00e0;
        public static final int email = 0x7f0a00ff;
        public static final int end_padder = 0x7f0a0103;
        public static final int forever = 0x7f0a0135;
        public static final int hybrid = 0x7f0a014e;
        public static final int icon = 0x7f0a014f;
        public static final int icon_group = 0x7f0a0151;
        public static final int icon_only = 0x7f0a0152;
        public static final int info = 0x7f0a0156;
        public static final int italic = 0x7f0a015a;
        public static final int light = 0x7f0a0170;
        public static final int line1 = 0x7f0a0171;
        public static final int line3 = 0x7f0a0172;
        public static final int media_actions = 0x7f0a01a3;
        public static final int none = 0x7f0a01e1;
        public static final int normal = 0x7f0a01e2;
        public static final int notification_background = 0x7f0a01e4;
        public static final int notification_main_column = 0x7f0a01e5;
        public static final int notification_main_column_container = 0x7f0a01e6;
        public static final int radio = 0x7f0a0230;
        public static final int right_icon = 0x7f0a023d;
        public static final int right_side = 0x7f0a023e;
        public static final int satellite = 0x7f0a024a;
        public static final int slide = 0x7f0a027a;
        public static final int standard = 0x7f0a028c;
        public static final int status_bar_latest_event_content = 0x7f0a0290;
        public static final int tag_transition_group = 0x7f0a02a1;
        public static final int terrain = 0x7f0a02a5;
        public static final int text = 0x7f0a02a8;
        public static final int text2 = 0x7f0a02a9;
        public static final int time = 0x7f0a02b3;
        public static final int title = 0x7f0a02b9;
        public static final int toolbar = 0x7f0a02cb;
        public static final int wide = 0x7f0a0312;
        public static final int wrap_content = 0x7f0a031a;
    }
    public static final class integer {
        private integer() {}

        public static final int cancel_button_image_alpha = 0x7f0b0004;
        public static final int google_play_services_version = 0x7f0b0009;
        public static final int status_bar_notification_info_maxnum = 0x7f0b0017;
    }
    public static final class layout {
        private layout() {}

        public static final int notification_action = 0x7f0d00c7;
        public static final int notification_action_tombstone = 0x7f0d00c8;
        public static final int notification_media_action = 0x7f0d00c9;
        public static final int notification_media_cancel_action = 0x7f0d00ca;
        public static final int notification_template_big_media = 0x7f0d00cb;
        public static final int notification_template_big_media_custom = 0x7f0d00cc;
        public static final int notification_template_big_media_narrow = 0x7f0d00cd;
        public static final int notification_template_big_media_narrow_custom = 0x7f0d00ce;
        public static final int notification_template_custom_big = 0x7f0d00cf;
        public static final int notification_template_icon_group = 0x7f0d00d0;
        public static final int notification_template_lines_media = 0x7f0d00d1;
        public static final int notification_template_media = 0x7f0d00d2;
        public static final int notification_template_media_custom = 0x7f0d00d3;
        public static final int notification_template_part_chronometer = 0x7f0d00d4;
        public static final int notification_template_part_time = 0x7f0d00d5;
    }
    public static final class string {
        private string() {}

        public static final int common_google_play_services_enable_button = 0x7f1200e7;
        public static final int common_google_play_services_enable_text = 0x7f1200e8;
        public static final int common_google_play_services_enable_title = 0x7f1200e9;
        public static final int common_google_play_services_install_button = 0x7f1200ea;
        public static final int common_google_play_services_install_text = 0x7f1200eb;
        public static final int common_google_play_services_install_title = 0x7f1200ec;
        public static final int common_google_play_services_notification_channel_name = 0x7f1200ed;
        public static final int common_google_play_services_notification_ticker = 0x7f1200ee;
        public static final int common_google_play_services_unknown_issue = 0x7f1200ef;
        public static final int common_google_play_services_unsupported_text = 0x7f1200f0;
        public static final int common_google_play_services_update_button = 0x7f1200f1;
        public static final int common_google_play_services_update_text = 0x7f1200f2;
        public static final int common_google_play_services_update_title = 0x7f1200f3;
        public static final int common_google_play_services_updating_text = 0x7f1200f4;
        public static final int common_google_play_services_wear_update_text = 0x7f1200f5;
        public static final int common_open_on_phone = 0x7f1200f6;
        public static final int common_signin_button_text = 0x7f1200f7;
        public static final int common_signin_button_text_long = 0x7f1200f8;
        public static final int fcm_fallback_notification_channel_label = 0x7f120140;
        public static final int status_bar_notification_info_overflow = 0x7f120228;
    }
    public static final class style {
        private style() {}

        public static final int TextAppearance_Compat_Notification = 0x7f13018a;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f13018b;
        public static final int TextAppearance_Compat_Notification_Info_Media = 0x7f13018c;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f13018d;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 0x7f13018e;
        public static final int TextAppearance_Compat_Notification_Media = 0x7f13018f;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f130190;
        public static final int TextAppearance_Compat_Notification_Time_Media = 0x7f130191;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f130192;
        public static final int TextAppearance_Compat_Notification_Title_Media = 0x7f130193;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f130273;
        public static final int Widget_Compat_NotificationActionText = 0x7f130274;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] FontFamily = { 0x7f040180, 0x7f040181, 0x7f040182, 0x7f040183, 0x7f040184, 0x7f040185 };
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] FontFamilyFont = { 0x1010532, 0x1010533, 0x101053f, 0x101056f, 0x1010570, 0x7f04017e, 0x7f040186, 0x7f040187, 0x7f040188, 0x7f040329 };
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int[] LoadingImageView = { 0x7f04009d, 0x7f0401a5, 0x7f0401a6 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] MapAttrs = { 0x7f04002a, 0x7f04006e, 0x7f04006f, 0x7f040070, 0x7f040071, 0x7f040072, 0x7f040073, 0x7f040074, 0x7f0401c8, 0x7f0401c9, 0x7f0401ca, 0x7f0401cb, 0x7f04021d, 0x7f040231, 0x7f04032a, 0x7f04032b, 0x7f04032c, 0x7f04032d, 0x7f04032e, 0x7f04032f, 0x7f040330, 0x7f040331, 0x7f040334, 0x7f040344 };
        public static final int MapAttrs_ambientEnabled = 0;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraMaxZoomPreference = 2;
        public static final int MapAttrs_cameraMinZoomPreference = 3;
        public static final int MapAttrs_cameraTargetLat = 4;
        public static final int MapAttrs_cameraTargetLng = 5;
        public static final int MapAttrs_cameraTilt = 6;
        public static final int MapAttrs_cameraZoom = 7;
        public static final int MapAttrs_latLngBoundsNorthEastLatitude = 8;
        public static final int MapAttrs_latLngBoundsNorthEastLongitude = 9;
        public static final int MapAttrs_latLngBoundsSouthWestLatitude = 10;
        public static final int MapAttrs_latLngBoundsSouthWestLongitude = 11;
        public static final int MapAttrs_liteMode = 12;
        public static final int MapAttrs_mapType = 13;
        public static final int MapAttrs_uiCompass = 14;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 16;
        public static final int MapAttrs_uiScrollGestures = 17;
        public static final int MapAttrs_uiScrollGesturesDuringRotateOrZoom = 18;
        public static final int MapAttrs_uiTiltGestures = 19;
        public static final int MapAttrs_uiZoomControls = 20;
        public static final int MapAttrs_uiZoomGestures = 21;
        public static final int MapAttrs_useViewLifecycle = 22;
        public static final int MapAttrs_zOrderOnTop = 23;
        public static final int[] SignInButton = { 0x7f040069, 0x7f0400ce, 0x7f04027f };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
