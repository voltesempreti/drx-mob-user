/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.drxmobpassageiro.user;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.drxmobpassageiro.user";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "1.0.9";
  // Fields from default config.
  public static final String BASE_IMAGE_URL = "https://drxmobpassageiro.com.br/storage/";
  public static final String BASE_PAY_URL = "https://drxmobpassageiro.com.br/index.php";
  public static final String BASE_URL = "https://drxmobpassageiro.com.br/";
  public static final String CLIENT_ID = "2";
  public static final String CLIENT_SECRET = "taHdKq3goXAkwavriUFZQVXbHag1AxyJseNCVgiE";
  public static final String DEVICE_TYPE = "android";
  public static final String DRIVER_PACKAGE = "com.drxmobpassageiro.driver";
  public static final String FCM_SERRVER_KEY = "AAAAIbY0xsk:APA91bGFzj_WrSrWXTSSuVhZMGj-Pq3tBuGO3tiHRJDyni9ck40x2i4tKRp0B5flBEpGVXmbiIhIAbOUpoVFAkYaCfTX4RdyYCEaQ0r23Q0JvhCEtk5fsCWMG_zKsrBrVbfXxkPbzvbn";
  public static final String HELP_URL = "https://drxmobpassageiro.com.br/help";
  public static final String PAYPAL_CLIENT_TOKEN = "123";
  public static final String TERMS_CONDITIONS = "https://drxmobpassageiro.com.br/privacy";
}
